#!/usr/bin/env sh
set -exu

# ** tasks that should be run before application is started
python3 -m pip install --upgrade pip wheel
python3 -m pip install --upgrade -r ./requirements.txt

# safety check
python3 -m pip install --upgrade safety
LC_ALL=C.UTF-8 LANG=C.UTF-8 safety check
