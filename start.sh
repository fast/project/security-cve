#!/usr/bin/env sh
set -exu

while :
do
# sloppy hack to loop over the code and pip metrics to influxdb
# TODO: use env vars for db; user and password
python3 ./security-cve.py | curl -XPOST 'http://influxdb:8086/write?db=securitydb&u=myuser&p=mypasswd' --data-binary @-
# wait 3 hours... dont get bannned from nvd.nist.gov
sleep 10800
done
