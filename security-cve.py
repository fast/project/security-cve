#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import subprocess
import time
import dateutil.parser
import requests
import yaml
import json
import logging
import os

logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.getLevelName(os.environ.get('APP_LOGLEVEL', 'INFO').upper()))

'''
if using debian: apt install python3-yaml python3-json python3-requests python3-dateutil
else: pip3 install yaml json requests dateutil
'''

CVSSV3_SEVERITY_MAPPING = {
    0: 'LOW',
    4: 'MEDIUM',
    7: 'HIGH',
    9: 'CRITICAL',
}

config_example = """
# one of [influxdb, json, yaml]
#output: influxdb

# should failed api calls set fail
#on_endpoint_error_count_fail: true
# wait 7 seconds before api calls
#seconds_to_sleep_before_api_call: 7
# wait 7 seconds before api calls
#seconds_to_sleep_after_failed_api_call: 30

# (optional) set proxy for connecting to endpoint
# you could also set environment HTTP_PROXY or HTTPS_PROXY
#proxies:
#  http: http://10.10.1.10:3128
#  https: http://10.10.1.10:1080

#cmd_shell: /bin/bash
cmd_shell: /bin/sh

# endpoint "nvd.nist.gov" only have the one.
nvd.nist.gov:
    # alias, host/app is the format used in the example
    salt.example.com/salt:
        # find your Common Platform Enumeration (CPE) name from nvd.nist.gov
        # https://nvd.nist.gov/products/cpe/search
        cpe: cpe:2.3:a:saltstack:salt:3000.2

    grep:
        cpe: cpe:/a:gnu:grep:2.5.3
        # (optional) shell command to check something
        # this example checks the version number to ensure it matches the CPE.
        # a return code other than 0 will set cmd_fail+=1
        # NOTE: if a CPE lookup fails this will set cmd_fail+=1
        cmd_test: grep --version | grep -q 'grep (GNU grep) 3.4'
        cmd_tests:
            - echo 'pass'
            - echo 'fails' | grep 'pass'

    grafana-prod/grafana:
        cpe: cpe:/a:grafana:grafana:6.4.3
        # https://nvd.nist.gov/vuln/search/results?cpe_version=cpe:/a:grafana:grafana:6.4.3
        # (optional) list CVE records to ignore. An ignored record wont be counted!
        # lets say we patched this CVE so we dont want it counted anymore.
        ignore:
            - CVE-2020-12245

    ubuntu18.04/openssl:
        cpe: cpe:/a:openssl:openssl:1.1.1
        # sometimes CPE versions are tricky to track down after a small patch
        # might just be more logical to ignore older CVE
        ignore_before: 2019-12-01
        # CVE-2019-1551
        # Published: December 06, 2019; 01:15:12 PM -05:00
"""
config = yaml.safe_load(config_example)

if os.path.exists('security-cve.yaml'):
    with open('security-cve.yaml', 'r') as f:
        config = yaml.safe_load(f.read())
elif os.path.exists('/etc/security-cve.yaml'):
    with open('/etc/security-cve.yaml', 'r') as f:
        config = yaml.safe_load(f.read())


class NvdNist_lookup(object):
    def __init__(self, url='https://services.nvd.nist.gov', proxies=None):
        self.url = url
        self.session = requests.Session()
        self.session.proxies = proxies

    def get(self, cpeMatchString, pubStartDate=None, **kwarg):
        query = '{0}/rest/json/cves/1.0?resultsPerPage=500'.format(self.url)
        query += '&cpeMatchString={0}'.format(cpeMatchString)
        if pubStartDate:
            query += '&pubStartDate={0}T00:00:00:000 UTC-00:00'.format(pubStartDate)
        for retry in range(1, 4):
            try:
                # NOTE: nvd.nist.gov has strict flooding protection.
                logger.debug(query)
                return self.session.get(query, timeout=30)
            except Exception as ex:
                logger.warning(ex)
                time.sleep(config.get('seconds_to_sleep_after_failed_api_call', 30))
                pass
        return None


verbose = {'nvd.nist.gov': {}}
lookup = NvdNist_lookup(proxies=config.get('proxies'))
for k, v in config.get('nvd.nist.gov', {}).items():
    # sleep to prevent hitting a strict flood block
    time.sleep(config.get('seconds_to_sleep_before_api_call', 7))

    count = {
        'fail': 0,
    }

    cmd_tests = v.get('cmd_tests', [])
    if v.get('cmd_test'):
        cmd_tests += [v.get('cmd_test')]
    for cmd in cmd_tests:
        try:
            logger.debug(cmd)
            ret = subprocess.run(cmd, timeout=15, shell=True, executable=config.get('cmd_shell', '/bin/sh'),
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            # print(ret)
            if ret.returncode:
                count['fail'] += 1
        except Exception as ex:
            logger.warning(ex)
            count['fail'] += 1

    cve_request = lookup.get(cpeMatchString=v['cpe'], pubStartDate=None)
    if not cve_request:
        if config.get('on_endpoint_error_count_fail', True):
            count['fail'] += 1
        logger.warning(cve_request)
        cve_request = None
    else:
        cve_request = cve_request.json()
        count.update({
            'total': 0,
            'ignored': 0,
            'not_ignored': 0,
            'severity_low': 0,
            'severity_medium': 0,
            'severity_high': 0,
            'severity_critical': 0,
        })
        ignore_list = [i.upper() for i in v.get('ignore', [])]
        ignore_before = None
        if v.get('ignore_before'):
            #ignore_before = datetime.datetime.fromisoformat(str(v.get('ignore_before')))
            ignore_before = dateutil.parser.parse(str(v.get('ignore_before')) + 'T00:00 UTC-00:00')

        for item in cve_request['result']['CVE_Items']:
            count['total'] += 1

            if item['cve']['CVE_data_meta']['ID'].upper() in ignore_list:
                count['ignored'] += 1
                continue

            if ignore_before and item.get('publishedDate'):
                # TODO: replace datetime hack '2019-12-06T18:15Z'.replace('Z','')
                #published_date = datetime.datetime.fromisoformat(str(item.get('publishedDate')).replace('Z',''))
                published_date = dateutil.parser.parse(str(item.get('publishedDate')))
                
                if ignore_before > published_date:
                    count['ignored'] += 1
                    continue

            # set the default cvssV3 severity to CRITICAL
            severity = 'CRITICAL'
            baseMetricV3 = item.get('impact', {}).get('baseMetricV3', {})
            baseMetricV2 = item.get('impact', {}).get('baseMetricV2', {})
            if baseMetricV3:
                severity = baseMetricV3.get('cvssV3', {}).get(
                    'baseSeverity', 'CRITICAL')
            elif baseMetricV2:
                # if we dont have a cvssV3 use cvssV2 mapped to cvssV3
                # this is not 100% correct but near the same...
                severity_score = baseMetricV2.get(
                    'cvssV2', {}).get('baseScore', 10)
                for s_lowerrange, s_name in sorted(CVSSV3_SEVERITY_MAPPING.items()):
                    if severity_score >= s_lowerrange:
                        severity = s_name
            count['severity_{0}'.format(severity.lower())] += 1
        count['not_ignored'] = count['total'] - count['ignored']

    if 'influxdb' in config.get('output', 'influxdb'):
        # output in influxdb line format.
        tags = {
            'source': 'nvd.nist.gov',
            'alias': k.replace(' ', '_').replace(',', '_'),
            'cpe': v['cpe'],
        }
        tags.update(v.get('tags',{}))
        tags = ','.join(['{0}={1}'.format(k, v) for k, v in tags.items()])
        values = ','.join(['{0}={1}'.format(k, v) for k, v in count.items()])
        metric = 'security_cve_count,{0} {1}'.format(tags, values)
        print(metric)

    verbose['nvd.nist.gov'][k] = {'config': v,'count': count, 'data': cve_request}


#if 'json' in config.get('output', ''):
#    print(json.dumps(verbose))
if 'yaml' in config.get('output', ''):
    print(yaml.dump(verbose, default_flow_style=False))
