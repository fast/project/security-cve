A docker WIP example that runs influxdb; grafana and a python script (security-cve.py)

docker-compose will bring the stack up so http://localhost:3000 is the login page. user: admin password: admin

the dashboard should be at: http://localhost:3000/d/security_check (might take ~30seconds for data to show up)


ISSUES
======

somthing seemm off with metrics.. datetime seems off. Perhaps dockers time is not set or in sync with system?
Perhaps need to set timezone...

Development workspaces
======================

See: .devops/README.md
