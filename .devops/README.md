Development workspaces
======================


Docker - Ununtu18.04
--------------------

packages
```
sudo apt install docker-compose
sudo usermod -a -G docker $USER
# reboot or use `su $USER` so `id` showes you in docker
```

config
```
# modify as needed
mkdir .local
cp env_docker env
cp security-cve.yaml_example security-cve.yaml
```

run
```
# (first time will take a while)
docker-compose up
# in another terminal in the sourcecode dir findout what port app is running on:
docker-compose port grafana 3000
## from a webbrowser open http://0.0.0.0:?????/
```

rebuild
```
docker-compose up --build
```

clean
```
docker-compose down
sudo git clean -Xdf
docker images
docker rmi ???_nginx
docker rmi ???_web
docker rmi ???_db
```
